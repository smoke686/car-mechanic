<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\helper;

use Yii;
use app\models\Sector;

/**
 * Description of Helper
 *
 * @author Андрей
 */
class Helper {

    //put your code here

    public static function getTime() {
        $session = Yii::$app->session;

        if (!$session->has('dateTime')) {
            
            date_default_timezone_set("UTC"); // Устанавливаем часовой пояс по Гринвичу
            $time = time();
            $offset = -5; // Допустим, у Торонто смещение относительно Гринвича составляет -5 часов
            $time += $offset * 3600; // Добавляем смещение по Гринвичу
            $dateShow = date("h:i", $time);
            
            $session->set('dateTime', $dateShow);
        } else {
            $dateShow = $session->get('dateTime');
        }
        return $dateShow;
    }

    public static function getSectors() {
        $session = Yii::$app->session;

        if (!$session->has('sectors')) {
            $sectors = Sector::find()->all();
            $session->set('sectors', $sectors);
        } else {
            $sectors = $session->get('sectors');
        }
        return $sectors;
    }

}
