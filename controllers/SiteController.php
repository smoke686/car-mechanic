<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Service;
use app\models\Reserve;
use app\models\Sector;
use yii\data\ActiveDataProvider;

class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $service = new Service();
        $reserve = new Reserve();

        if ($service->load(Yii::$app->request->post()) && $service->upload() && $service->save()) {
            $service = new Service();
        }

        if ($reserve->load(Yii::$app->request->post()) && $reserve->save()) {
            $reserve = new Reserve();
        }

        $list = new ActiveDataProvider([
            'query' => Reserve::find(),
        ]);

        return $this->render('index', [
                    'service' => $service,
                    'reserve' => $reserve,
                    'list' => $list,
        ]);
    }

    public function actionImage($id) {
        $reserve = Reserve::findOne($id);
        if (isset($reserve)) {
            header("Content-type: image/jpeg");
            echo $reserve->image;
        } else {
            echo 'Картинки не существует';
        }
    }

    public function actionDate() {
        $session = Yii::$app->session;
        if ($session->has('dateTime')) {
            $dateShow = date('h:i', strtotime($session->get('dateTime') . ' +1 min'));
            $session->set('dateTime', $dateShow);
        }
        return $this->renderAjax('_formDate');
    }
	
	public function actionSector($id) {
        // Нужно бы попровить место
        $model = Sector::findOne($id);

        if (isset($model) && $model->load(Yii::$app->request->post()) && $model->save()) {
            $session = Yii::$app->session;
            $sectors = Sector::find()->all();
            $session->set('sectors', $sectors);
            
            return $this->renderAjax('_sector', [
                        'model' => $model,
            ]);
        }
    }
}
