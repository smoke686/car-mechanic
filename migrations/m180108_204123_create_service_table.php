<?php

use yii\db\Migration;

/**
 * Handles the creation of table `service`.
 */
class m180108_204123_create_service_table extends Migration {

    /**
     * @inheritdoc
     */
    public function up() {      
        $this->createTable('service', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->null(),
            'duration' => $this->integer()->null(),
            'image' => 'LONGBLOB NULL',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('service');
    }

}
