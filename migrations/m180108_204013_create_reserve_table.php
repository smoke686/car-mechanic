<?php

use yii\db\Migration;

/**
 * Handles the creation of table `reserve`.
 */
class m180108_204013_create_reserve_table extends Migration {

    /**
     * @inheritdoc
     */
    public function up() {  
        $this->createTable('reserve', [
            'id' => $this->primaryKey(),
            'service_id' => $this->integer()->null(),
            'date_time' => $this->dateTime()->null(),
            'image' => 'LONGBLOB NULL',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('reserve');
    }

}
