<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sector`.
 */
class m180108_204108_create_sector_table extends Migration {

    /**
     * @inheritdoc
     */
    public function up() {     
        $this->createTable('sector', [
            'id' => $this->primaryKey(),
            'name' => $this->string(20)->null(),
            'hour' => $this->integer()->null(),
            'period_min' => $this->integer()->null(),
        ]);
        
        $this->insert('sector', ['name' => 'sec1', 'hour' => 0, 'period_min' => 35]);
        $this->insert('sector', ['name' => 'sec2', 'hour' => 3, 'period_min' => 50]);
        $this->insert('sector', ['name' => 'sec3', 'hour' => 6, 'period_min' => 30]);
        $this->insert('sector', ['name' => 'sec4', 'hour' => 9, 'period_min' => 50]);
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('sector');
    }

}
