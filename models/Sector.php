<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sector".
 *
 * @property integer $id
 * @property string $name
 * @property integer $hour
 * @property integer $period_min
 */
class Sector extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sector';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hour'], 'integer', 'min' => 1, 'max' => 24],
            [['period_min'], 'integer', 'min' => 30, 'max' => 150],
            [['name'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название сектора',
            'hour' => 'Прибавка в часах в текущему времени (в часах)',
            'period_min' => 'Pn - в минутах (30 - 150)',
        ];
    }
    
    public function getStart($time){
        return date('H:i', strtotime($time . ' +' . $this->hour . ' hours'));
    }
    
    public function getEnd($time)
    {
        return date('H:i', strtotime($time . ' +' . $this->hour . ' hours +' . $this->period_min . ' min'));
    }
}
