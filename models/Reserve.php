<?php

namespace app\models;

use Yii;
use app\models\Service;
use app\helper\TextOnImage;
use app\models\Sector;
use app\helper\Helper;

/**
 * This is the model class for table "reserve".
 *
 * @property integer $id
 * @property integer $service_id
 * @property string $date_time
 * @property resource $image
 */
class Reserve extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'reserve';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['service_id'], 'integer'],
            [['date_time'], 'safe'],
            [['image'], 'string'],
            [['service_id', 'date_time'], 'required'],
        ];
    }

    public function writeText() {
        $service = $this->service;

        if (isset($service, $service->name, $service->image)) {
            $file = $service->image;
            $ttfImg = new TextOnImage($file, true);

            $ttfImg->ttfFontDir = \Yii::getAlias('@webroot') . '/font';
            //Название услуги
            $name = $service->name;
            $date = date('H:i', strtotime($this->date_time)) . ' - ' . date('H:i', strtotime($this->date_time . ' +' . $this->service->duration . ' min'));

            $fontSize = round($ttfImg->getImageWidth() / mb_strlen($name)) + 5;
            $ttfImg->setFont('11143.ttf', $fontSize, "#800000", 40);

            //Шрифт для даты
            $fontSize = round($ttfImg->getImageWidth() / (mb_strlen($date) - 4)) + 5;
            $ttfImg->writeTextCenter($name, round(($fontSize) / 2) * -1 - 1);

            $ttfImg->setFont('11143.ttf', $fontSize, "#800000", 40);
            $ttfImg->writeTextCenter($date, round(($fontSize) / 2) + 1);

            //Сохраняем в базу картинку
            $this->image = $ttfImg->getImageString();
            return $this->image !== false;
        }
        $this->addError('image', 'Не удалось определить услугу.');
        return false;
    }

    public function checkDate() {
        $count = Reserve::find()
                ->where(['>=', 'date_time', $this->date_time])
                ->andWhere(['<=', 'date_time', date('Y-m-d H:i:s', strtotime($this->date_time . ' +' . $this->service->duration . ' min'))])
                ->count();
        if ($count > 0) {
            $this->addError('date_time', 'В данное время запись невозможна. Пересечение с другими записями.');
            return false;
        }

        if ((int) date('G', strtotime($this->date_time)) < 9) {
            $this->addError('date_time', 'В данное время запись невозможна. Мастер работает с 9:00.');
            return false;
        }

        if ((int) date('G', strtotime($this->date_time)) > 21) {
            $this->addError('date_time', 'В данное время запись невозможна. Мастер работает до 21:00.');
            return false;
        }

        if (date('H:i', strtotime($this->date_time . ' +' . $this->service->duration . ' min')) > date('H:i', strtotime('21:00'))) {
            $this->addError('date_time', 'В данное время запись невозможна. Мастер не успеет выполнить услугу, рабочий день до 21.');
            return false;
        }

        $sectors = Sector::find()->all();
        $time = Helper::getTime();
        foreach ($sectors as $sector) {
            if (date('H:i', strtotime($time . ' +' . $sector->hour . ' hours')) <= date('H:i', strtotime($this->date_time)) && date('H:i', strtotime($time . ' +' . $sector->hour . ' hours +' . $sector->period_min . ' min')) >= date('H:i', strtotime($this->date_time))) {
                $this->addError('date_time', 'Отказа бронирования, сектор ' . $sector->name);
                return false;
            }
        }

        return true;
    }

    public function validate($attributeNames = null, $clearErrors = true) {
        $validate = parent::validate($attributeNames, $clearErrors);
        return $validate && $this->checkDate() && $this->writeText();
    }

    //Получить услугу на которую зарезервировались
    public function getService() {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    //Получить список услуг
    public function getServices() {
        return Service::find()->all();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'service_id' => 'Услуга',
            'date_time' => 'Дата и время',
            'image' => 'Изображение',
        ];
    }

}
