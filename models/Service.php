<?php

namespace app\models;

use Yii;
use app\models\Reserve;
use yii\web\UploadedFile;

/**
 * This is the model class for table "service".
 *
 * @property integer $id
 * @property integer $name
 * @property integer $duration
 * @property resource $image
 */
class Service extends \yii\db\ActiveRecord {

    public $upload = null;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'service';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['duration'], 'integer', 'min' => 1, 'max' => 720],
            [['image'], 'string'],
            [['image'], 'safe'],
            ['upload', 'image', 'extensions' => 'png, jpg, JPG, PNG',
                'minWidth' => 50, 'maxWidth' => 2000,
                'minHeight' => 50, 'maxHeight' => 2000,
                'maxSize' => 2000 * 2000
            ],
            [['name'], 'string', 'max' => 255],
            [['name', 'duration', 'upload'], 'required'],
        ];
    }

    public function getReserve() {
        return $this->hasMany(Reserve::className(), ['service_id' => 'id']);
    }

    public function upload() {
        if (isset($this->upload)) {
            $this->upload = UploadedFile::getInstance($this, 'upload');
            $this->image = file_get_contents($this->upload->tempName);
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'duration' => 'Длительность в минутах',
            'image' => 'Изображение',
            'upload' => 'Загружаемый файл',
        ];
    }

}
