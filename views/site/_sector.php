<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Sector */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sector-form">
    <?php Pjax::begin(['enablePushState' => false, 'id' => 'sector_'.uniqid()]); ?>
    <?php
    $form = ActiveForm::begin([
                'action' => ['/site/sector', 'id' => $model->id],
                'id' => 'sectorForm_'.uniqid(),
                'options' => [
                    'data-pjax' => true,
                ]
    ]);
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hour')->textInput() ?>

    <?= $form->field($model, 'period_min')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>