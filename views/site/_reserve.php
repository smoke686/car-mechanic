<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Reserve */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs(
        '$("document").ready(function(){
            $("#reserveForm").on("pjax:end", function() {
            $.pjax.reload({container:"#reserveList"});  //Reload GridView
        });
    });'
);
?>

<div class="reserve-form">
    <?php Pjax::begin(['id' => 'reserveForm']) ?>
    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'data-pjax' => true,
                ]
    ]);
    ?>

    <?= $form->field($model, 'service_id')->dropDownList(ArrayHelper::map($model->services, 'id', 'name')) ?>

    <?=
    $form->field($model, 'date_time')->widget(
            'trntv\yii\datetime\DateTimeWidget', [
        'phpDatetimeFormat' => 'dd/MM/yyyy HH:mm:ss',
        'momentDatetimeFormat' => 'YYYY-MM-DD HH:mm'
            ]
    );
    ?>

    <?= $form->errorSummary($model) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>
