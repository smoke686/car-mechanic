<?php

use yii\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\Pjax
?>
<div class="row">
    <h1 class="text-center">Запись к Канадскому Автослесарю</h1>
</div>
<div class="row">
    <div class="col-xs-12">
        <?=
        $this->render('_formDate')
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-6">
        <h2 class="text-center">Добавить услугу</h2>
        <?=
        $this->render('_service', [
            'model' => $service,
        ])
        ?>
    </div>
    <div class="col-xs-6">
        <h2 class="text-center">Записаться</h2>
        <?=
        $this->render('_reserve', [
            'model' => $reserve,
        ])
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <h2 class="text-center">Список забронированных</h2>
        <?php if (isset($list)): ?>
            <?php Pjax::begin(['id' => 'reserveList', 'enablePushState' => false]) ?>
            <?=
            GridView::widget([
                'dataProvider' => $list,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'label' => 'Название услуги',
                        'attribute' => 'service_id',
                        'content' => function($data) {
                            return $data->service->name;
                        }
                    ],
                    [
                        'label' => 'Дата и время регистрации',
                        'attribute' => 'date_time',
                    ],
                    [
                        'label' => 'Картинка',
                        'format' => 'raw',
                        'value' => function($data) {
                            return Html::a(
                                            'Перейти', Url::to(['site/image', 'id' => $data->id]), [
                                        'target' => '_blank',
                                        'data-pjax' => '0'
                                            ]
                            );
                        }
                            ],
                        ],
                    ]);
                    ?>
                    <?php Pjax::end(); ?>
                <?php endif; ?>
    </div>
</div>