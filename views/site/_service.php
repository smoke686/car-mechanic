<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Service */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs(
        '$("document").ready(function(){
            $("#serviceForm").on("pjax:end", function() {
            $.pjax.reload({container:"#reserveForm"});  //Reload GridView
        });
    });'
);
?>

<div class="service-form">
    <?php Pjax::begin(['id' => 'serviceForm']) ?>
    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'data-pjax' => true,
                ]
    ]);
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'duration')->textInput() ?>

    <?= $form->field($model, 'upload')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?> 
    <?php Pjax::end();  ?>
</div>
