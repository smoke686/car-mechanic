<?php

use yii\widgets\Pjax;
use yii\bootstrap\Html;
use app\helper\Helper;
use yii\bootstrap\Modal;

$sectors = Helper::getSectors();
$time = Helper::getTime();
?>


<?php
$script = <<< JS
$(document).ready(function() {
    setInterval(function(){  $("#refreshButton").click();}, 1000);
});
JS;
$this->registerJs($script);
?>

<?php Pjax::begin(['id' => 'formDate', 'enablePushState' => false]); ?>
<?= Html::a("Обновить", ['site/date'], ['id' => 'refreshButton', 'class' => 'hidden']); ?>
<h3 class="text-center">Сейчас: <?= $time ?></h3>

<div class="row">
    <?php if (isset($sectors) && is_array($sectors)): ?>
        <h4 class="text-center">Сектора отказа</h4>
        <?php foreach ($sectors as $sector): ?>
            <div class="col-xs-6 text-center">
                <h4><?= $sector->name ?> : <?= $sector->getStart($time) ?> - <?= $sector->getEnd($time) ?></h4>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<?php Pjax::end(); ?>

<div class="row">
    <?php if (isset($sectors) && is_array($sectors)): ?>
        <h4 class="text-center">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalSectorsEdit">Редактирование секторов</button>
        </h4>
        <?php
        Modal::begin([
            'header' => '<h3 class="text-center">Редактирование секторов</h2>',
            'id' => 'modalSectorsEdit'
        ]);
        ?>
        <div class="row">
            <?php foreach ($sectors as $sector): ?>
                <div class="col-xs-6">
                    <?=
                    $this->render('_sector', [
                        'model' => $sector,
                    ])
                    ?>
                </div>
            <?php endforeach; ?>
        </div>
        <?php Modal::end(); ?>

    <?php endif; ?>
</div>
